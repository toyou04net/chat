# 신입 첫 번째 응용 과제 - Chat

## Chat 이란?

Chat은 Server와 Client 간의 1:1 채팅이 가능한 Chatting Program 이다.

본 프로그램은 첫 번째 과제 Wtiin(What-time-is-it-now)를 응용한 채팅프로그램으로써
JAVA를 이용한 서버와 클라이언트간의 네트파일 프로그래밍 기초 이해평가 및 알고리즘에 관한 기초 상식,
협업 업무 환경에 관한 경험 평가를 하기 위한 목적으로 개발된 프로그램이다.

## Getting Started

[Chat.zip 압축파일](https://bitbucket.org/toyou04net/chat/downloads/Chat.zip)을 다운로드 하고
압축을 해제한다.

참고) Chat.zip 파일의 구성

- ChatServer.bat : 서버 어플리케이션 실행 스크립트
- ChatClient.bat : 클라이언트 어플리케이션 실행 스크립트
- Chat.jar : 프로그램 패키지

### 서버를 실행하는 방법

1. CLI(Command Line Interface) 혹은 GUI(Graphical User Interface)에서 ChatServer.bat 파일을 실행한다.
2. 서버 어플리케이션은 **IP Address**를 출력한다. 출력된 IP를 클라이언트에게 알려준다.
3. 클라이언트의 정상적인 접속 시도시 1:1 연결이 된다.
4. 클라이언트로 부터 수신할 메세지를 기다린다.
5. 메세지 수신후 보낼 메세지를 입력한다.
6. 4와 5를 반복한다.

### 클라이언트를 실행하는 방법

1. CLI(Command Line Interface) 혹은 GUI(Graphical User Interface)에서 ChatClient.bat 파일을 실행한다.
2. **IP 입력란**에 서버의 **IP Address**를 입력한다.
3. 서버에 보낼 메세지를 입력한다.
4. 서버로부터 회신을 기다린다.
5. 3과 4를 반복한다.

### 구동 화면
참고) 좌 : ChatServer.bat 우 : ChatClient.bat
![구동 화면](https://bitbucket.org/toyou04net/chat/downloads/readmeimg.jpg)

--------------------
엔에이티(NAT) 배도영 
toyou04net@nat21c.com 010-3201-1591
