package nat21c.chat.server;

public class ChatServerMain {

    private static Integer PORT = 7777; // 포트를 7777로 설정한다.

    /**
     * ChatServer 서버 메인 클래스이다.
     * ChatServer 인스턴스를 생성하고 실행한다.
     */
    public static void main(String args[]) {
        try {
            ChatServer chatserver = new ChatServer(PORT);
            chatserver.start();
        } catch (Exception e) {
            System.out.println("실행 오류 : " + e.getMessage());
        }
    }
}
