package nat21c.chat.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import nat21c.chat.common.ChatCommon;


/**
 * Chat 프로그램의 서버를 준비한다.
 */
public class ChatServer {

    /*
     * socket : 서버에 접속시 생성할 소켓변수
     * port : 서버에 접속시 필요한 port
     */
    private int port;
    private Socket socket;

    /**
     * 서버 환경 설정하는 생성자.
     * @param port : 서버 환경 설정에 필요한 변수 (현재설정 : 7777)
     */
    public ChatServer(int port) {
        this.port = port;
    }

    /**
     * ChatServer.bat를 실행하는 시스템의 IP를 출력한다.
     * 서버 소켓을 생성한다.
     * 서버와 클라이언트간의 1:1 채팅에 필요한 스트림을 생성한다.
     * 서버와 클라이언트간의 1:1 채팅을 while문을 이용하여 반복한다.
     */
    public void start() {
        printServerIp();
        createServerSocket();

        ChatCommon chatcommon = new ChatCommon(socket);

        while(true) {
            chatcommon.inputChatView();
            chatcommon.outputChatSend();
        } 
    }

    /**
     * 서버를 가동한 현재 시스템의 IP주소를 출력한다.
     */
    public void printServerIp() {
        try {
            InetAddress ipaddress = InetAddress.getLocalHost();
            System.out.println(ipaddress);
        } catch (IOException e) {
            throw new RuntimeException("네트워크 오류 : " +e.getMessage());
        }
    }

    /**
     * 서버 소켓을 생성한다.
     * 소켓의 연결 요청시 승인한다.
     * 승인된 소켓의 (현재시간 + 소켓의해당ip + 님이 입장하셨습니다.) 라는 메세지를 출력한다.
     * @return socket : 클라이언트 연결요청이 승인되어 생성된 소켓을 반환한다.
     */
    public Socket createServerSocket() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println(getTime() + "소켓 설정 완료. 소켓 : " + port);
            System.out.println("새로운 연결요청을 기다리는 중입니다.");
            socket = serverSocket.accept();
            System.out.println(getTime() + socket.getInetAddress() + "님이 입장하셨습니다.");
            return socket;
        } catch (IOException e) {
            throw new RuntimeException("네트워크 오류 : " +e.getMessage());
        }
    }

    /**
     *양식화된 문자열 현재시각의
     * @return 현재시각. yyyy-MM-dd hh:mm:ss
     */
    String getTime() {
        SimpleDateFormat f = new SimpleDateFormat("[hh:mm:ss]");
        return f.format(new Date());
    }
}
