package nat21c.chat.common;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * 통신에 필요한 Steram 을 생성하고, 수신 및 전송한다.
 */
public class ChatCommon {


    /*
     * socket : 통신에 필요한 소켓
     * DataOutputStream : 메시지 송신에 필요한 스트림
     * DataInputStream : 수신에 필요한 스트림
     */ 
    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;

    /**
     * 클라이언트(혹은 서버)로부터 받은 소켓을 저장한다.
     * 수신에 필요한 Stream을 생성한다.
     * 출력에 필요한 Stream을 생성한다.
     * @param socket : 클라이언트(혹은 서버)로부터 받은 소켓 및 통신에 사용될 소켓
     */
    public ChatCommon(Socket socket) {
        this.socket = socket;
        this.createInputStream(this.socket);
        this.createOutputStream(this.socket);
    }
    /**
     * 클라이언트가 서버(혹은 반대)에게 대화내용을 수신받기위한 메서드로,
     * 수신에 필요한 Stream 을 생성한다.
     * @param socket : 클라이언트(혹은 서버)로부터 받은 소켓 및 통신에 사용될 소켓
     */
    private void createInputStream(Socket socket) {
        try {
            dataInputStream = new DataInputStream(socket.getInputStream());
            //return new  DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException("InputStream 생성 오류 : " +e.getMessage());
        }
    }

    /**
     * 서버 (혹은 클라이언트)로부터 수신한 메세지를 출력한다.
     */
    public void inputChatView() {
        try {
            System.out.println("받은 메세지: " + dataInputStream.readUTF());
        } catch (IOException e) {
            throw new RuntimeException("InputStream 수신 오류 : " +e.getMessage());
        }
    }

    /**
     * 클라이언트(혹은 서버)가 입력한 메세지를 서버에게 전송하기 위한 메서드로,
     * 전송에 필요한 Stream 을 생성한다.
     * @param socket : 클라이언트(혹은 서버)로부터 받은 소켓 및 통신에 사용될 소켓
     */
    private void createOutputStream(Socket socket) {
        try {
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            //return new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException("OutputStream 생성 오류 : " +e.getMessage());
        }
    }

    /**
     * 서버 (혹은 클라이언트)에게 전송할 메세지를 입력받는다.
     */
    public void outputChatSend() {
        System.out.println("내용을 입력해주세요.");
        Scanner scan = new Scanner(System.in);
        String content = scan.nextLine();
        try {
            dataOutputStream.writeUTF(content);
            System.out.println(socket.getInetAddress() + "에게 데이터의 전송을 완료하였습니다. ");
        } catch (IOException e) {
            throw new RuntimeException("OutputStream 송신 오류 : " +e.getMessage());
        } 
    }
}
