package nat21c.chat.client;

/**
 * 개발자가 정의한 예외 클래스이다. 
 */
public class ConnectServerFail extends Exception {

    public ConnectServerFail() {

    }

    /**
     *  Ip입력 후 서버 접속시 에러를 검출한다.
     */
    public ConnectServerFail(String s) {
        super("사용자 정의 에러 발생 : " + s);
    }
}
