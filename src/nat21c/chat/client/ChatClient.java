package nat21c.chat.client;

import java.net.Socket;
import java.util.Scanner;
import java.util.regex.Pattern;

import nat21c.chat.common.ChatCommon;

/**
 * Chat 프로그램의 클라이언트를 준비한다.
 */
public class ChatClient {

    /* 
     * serverip : 서버접속시 필요한 serverip를 사용자용과 개발자용으로 분리함.
     *            사용자용은 직접 입력을 받아 저장하고, 개발자용은 해당ip를 사용함.
     * socket : 서버에 접속시 생성할 소켓변수
     */
    private String serverip;
    //private String serverip ="192.168.10.125";
    private Socket socket;

    /**
     * 사용자에게 ip 입력을 받는다.
     * 서버에 접속 성공시 소켓을 생성한다.
     * 서버와 클라이언트간의 1:1 채팅에 필요한 스트림을 생성한다.
     * 서버와 클라이언트간의 1:1을 while문을 이용하여 반복한다.
     * @throws ConnectServerFail : 서버 접속 실패시 예외 메세지를 출력한다.
     */
    public void start() throws ConnectServerFail {

        scanIpaddress();

        try {
            createSocket();
        } catch (Exception e) {
            throw new ConnectServerFail("서버에 접속할 수 없습니다. 다시 시도해주세요.");
        }

        ChatCommon chatcommon = new ChatCommon(socket);
        while (true) {
            chatcommon.outputChatSend();
            chatcommon.inputChatView();
        }
    }

    /**
     * 사용자로부터 받아온 serverip와 7777포트를 이용하여 서버 접속이 성공한다면 소켓을 생성한다.
     * 서버 접속이 실패시 실패이유를 출력한다.
     * @return socket : 서버에 연결요청을 시도하여 정상적인 접속시 생성된 소켓을 반환한다.
     * @throws ConnectServerFail : 서버 접속 실패시 예외 메세지를 출력한다.
     */
    public Socket createSocket() throws ConnectServerFail {
        try {
            socket = new Socket(serverip, 7777);
            System.out.println("서버 접속에 성공하였습니다..");
            return socket;
        } catch (Exception e) {
            throw new ConnectServerFail();
        }
    }

    /**
     * 서버 접속에 필요한 ipaddress를 입력받는다.
     * 정규식을 통해 아이피형식의 입력을 판별하고 잘못 되었을 경우 재입력 받는다.
     * @return 입력받은 ip주소
     */
    private String scanIpaddress() {
        Pattern pattern;
        String regex = "((([0-9])|([1-9]\\d{1})|(1\\d{2})|(2[0-4]\\d)|(25[0-5]))\\.){3}(([0-9])|([1-9]\\d{1})|(1\\d{2})|(2[0-4]\\d)|(25[0-5]))";
        pattern = Pattern.compile(regex);

        while(true) {

            System.out.println("서버에 접속할 ip를 입력해주세요.");
            Scanner scan = new Scanner(System.in);
            serverip = scan.nextLine();

            if(pattern.matcher(serverip).matches())
            {
                System.out.println("아이피형식이 맞습니다. 접속을 시도합니다.");
                break;
            }
            else
            {
                System.err.println("입력이 잘못 되었습니다. 다시 입력해주세요.");
            }
        }
        return serverip;
    }
}
