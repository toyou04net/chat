package nat21c.chat.client;


public class ChatClientMain {
    /**
     * ChatClient 클라이언트 메인 클래스이다.
     * ChatClient 인스턴스를 생성하고 실행한다.
     */
    public static void main(String args[]) {

        while(true) {
            try {
                ChatClient chatclient = new ChatClient();
                chatclient.start();
            } catch (ConnectServerFail e) {
                System.out.println("실행 오류 : " + e.getMessage());
            }
        }
    }
}
